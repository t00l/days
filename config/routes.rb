Rails.application.routes.draw do
  root to: "projects#new"
  resources :projects
  resources :tasks do
    get :split, on: :member
  end
  resources :sub_tasks
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
