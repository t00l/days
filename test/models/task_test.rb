require 'test_helper'

class TaskTest < ActiveSupport::TestCase
  require "application_system_test_case"

  test 'split' do
    #projects
    data_1 = {name: 'Project IPS 1', workingdays: "[1,2,3,4,5]"}
    @project_1 = Project.create data_1
    data_2 = {name: 'Project IPS 2', workingdays: "[1,2,3]"}
    @project_2 = Project.create data_2
    data_3 = {name: 'Project IPS 3', workingdays: "[3,4,5,6,7,1,2]"}
    @project_3 = Project.create data_3
    #tasks
    data_4 = {name:'Task IPS 1', project_id: @project_1.id}
    @task_1 = Task.create data_4
    data_5 = {name:'Task IPS 2', project_id: @project_2.id}
    @task_2 = Task.create data_5
    data_6 = {name:'Task IPS 3', project_id: @project_3.id}
    @task_3 = Task.create data_6
    #splits
    @task_1.split
    @task_2.split
    @task_3.split
    assert_equal [1,2,3,4,5], @task_1.subtasks.map(&:weekday)
    assert_equal [1, 2, 3, 1, 2], @task_2.subtasks.map(&:weekday)
    assert_equal [3, 4, 5, 1, 2], @task_3.subtasks.map(&:weekday)
  end

end
