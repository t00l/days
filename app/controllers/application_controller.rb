class ApplicationController < ActionController::Base
  before_action :set_locale
  
  def set_locale
    I18n.config.available_locales = :es
  end
  
end
