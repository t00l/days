class SubTask < ApplicationRecord
  belongs_to :task, optional: true, class_name: 'Task'
end
