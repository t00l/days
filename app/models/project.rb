class Project < ApplicationRecord
  #serializar la semana
  serialize :workingdays,Array
  #asociación
  has_many :tasks, dependent:  :destroy
  #nested
  accepts_nested_attributes_for :tasks

  def workingdays=(value)
    super(value.scan(/\d+/).map(&:to_i))
  end

end
