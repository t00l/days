class Task < ApplicationRecord
  #asociación
  belongs_to :project
  has_many :subtasks, class_name: 'SubTask', foreign_key: :task_id, dependent: :destroy
  #nested
  accepts_nested_attributes_for :subtasks

  #method split
  def split
    days = project.workingdays
    days.slice!(3..4) if days.include?(6)
    (0..4).each_with_index{|data, index| subtasks << SubTask.new(name: "ST#{index+1}", weekday: days[data % days.size]) }
  end
end
